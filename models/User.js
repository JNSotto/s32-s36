 const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

	firstName:{
		type: String,
		required: [true, 'Please Enter Your FirstName']
	},

	lastName:{
		type: String,
		required: [true, 'Please Enter Your LastName']
	},

	email:{
		type: String,
		required: [true, 'Please Enter Your Email']
	},

	password:{
		type: String,
		required: [true, 'Please Enter Your Password']
	},
	
	isAdmin:{
		type:Boolean,
		default: false
	},

	mobileNo:{
		type: String,
		required: [true, 'Please Enter Your Mobile Number']
	},
	
	enrollments:[
		{
			courseId: {
				type: String,
				required: [true, 'Please Enter the Course ID']
			},

			enrolledOn:{
				type:Date,
				default: new Date()
			},
			status:{
				type: String,
				default: "enrolled"
			}
		}
	]
});

module.exports = mongoose.model('User', userSchema)