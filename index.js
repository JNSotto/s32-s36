 const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course")

const port = process.env.PORT || 4000;//process.env.PORT is for future deployment, not in your localhost.

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

/*

const dbName = "course-booking"
const dbUser = "_____"
const dbPass = "_____"
const url = `mongodb+srv://${dbUser}:${dbPass}@personaldatabase.gqla2.mongodb.net/${dbName}?retryWrites=true&w=majority`;
 */
mongoose.connect('mongodb+srv://admin:admin1234@zuittbootcamp.p5gi4.mongodb.net/course-booking?retryWrites=true&w=majority', 
	{

		useNewUrlParser: true,
		useUnifiedTopology:true
	}
);

let db = mongoose.connection;
	

	db.on("error",console.error.bind(console,"Connection Error"));
	db.once('open',() => console.log('Connected to MongoDB Atlas Cloud Database'));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}.`));