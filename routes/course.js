const express = require('express');
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth")

//Activity s34 - Add Course with Authorization 
/*router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin === true){
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send('Not Authorized')
	}
});*/

//Activity s34 Solution
router.post("/", auth.verify, (req, res) => {		
	const userData = auth.decode(req.headers.authorization);

	courseController.addCourse(userData, req.body).then(resultFromController => 
		res.send(resultFromController))
});


//Retrieve all the courses
router.get("/all", (req,res)=>{
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

//Retrieve all active course
router.get("/", (req,res)=>{
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

//Retrieve the specific course
router.get("/:courseId", (req, res)=>{
	console.log(req.params)
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController=> res.send(resultFromController));
})

//Update a course
router.put("/:courseId", auth.verify, (req, res)=>{
	courseController.updateCourse(req.params, req.body).then(resultFromController=> res.send(resultFromController));
})

//Activity s35
//Archiving a course
router.put("/:courseId/archive", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true){
	courseController.updateCourse(req.params, req.body).then(resultFromController=> res.send(resultFromController));
	} else {
		return res.send('Not Authorized')
	}
})

module.exports = router;  
