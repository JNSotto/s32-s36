 const express = require('express');
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Activity s33 

/*router.get("/details", auth.verify,(req, res) => {

	const userData = auth.decode(req.headers.authorization)
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
}); */


//Solution
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	
	userController.getProfile({userId: req.body.id}).then(resultFromController => 
		res.send(resultFromController))
})

//For enrolling a user
router.post("/enroll",auth.verify,(req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	let data = {
		userId: req.body.userId,
		courseId:req.body.courseId
	}

	if(userData.isAdmin == false){
		userController.enroll(data).then(resultFromController => 
			res.send(resultFromController))
	} else {
		res.send('Admin is Not Allow to Enroll')
	}
})

module.exports = router;
