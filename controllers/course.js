const Course = require("../models/Course");

//Activity s34 - Add Course with Authorization 
/*module.exports.addCourse = (reqBody) => {
	
	let newCourse = new Course({
		name:reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	})

	return newCourse.save().then((course,err) =>{
		if(err) {
			return false
		} else {
			return true
		}
	})

} */

//Activity s34 Solution 
module.exports.addCourse = async (user, reqBody) => {
	if(user.isAdmin == true){
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return ("Not Authorized");
	}
}


//Retrieve all the courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result =>{
		return result;
	})
}

//Retrieve all active courses
module.exports.getAllActive = () =>{
	return Course.find({isActive:true}).then(result =>{
		return result;
	})
}

//Retrieve specific course
module.exports.getCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then(result=> {
		let course = {
		name:reqBody.name,
		description:reqBody.description,
		price: reqBody.price
	}
	return course;
	})
}

module.exports.updateCourse = (reqParams,reqBody) =>{
	let updatedCourse = {
		name:reqBody.name,
		description:reqBody.description,
		price: reqBody.price
	}

	//Syntax: findByIdAndUpdate(document, updateToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,err)=>{
		if(err){
			return false
		} else {
			return true
		}
	})
}

//Activity s35
// Archiving a course
module.exports.updateCourse = (reqParams,reqBody) =>{
	let archiveCourse = {
		isActive: reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course,err)=>{
		if(err){
			return false
		} else {
			return true
		}
	})
}
